[toc]

### vic-environment

#### 介绍
>  旨在快速搭建开发环境

#### 子项目目录说明

##### [1 parent-util-dependencies](/parent-util-dependencies)

> 一些常用工具类的依赖聚合
>
> 如:
>
> commons
>
> poi



##### [2 parent](/parent)

> 作为一个maven项目的父项目,定义了一些常用的依赖,并依赖了parent-util-dependencies

##### [3 vic-boot-archtype](/vic-boot-archtype)

>  [如何自定义maven archtype](vic-boot-archtype)

> 一个maven的archtype,方便生成模板项目



[4 parent-mvc](/parent-mvc)

> springmvc 项目，其实已经被淘汰了，只是偶尔老项目中有，所以搭建个配套测试的环境