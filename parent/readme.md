[toc]

### 项目的公共依赖

#### 1 统一依赖版本

- jdk版本
- 文件编码
- 依赖版本
- 插件版本



#### 2统一开启代码检测插件 TODO

- checkstyle
- jaccoco
- spotbugs
- fat jar
- sonar



跳过sonar

```
mvn verify -Dsonar.skip=true
mvn install -Dsonar.skip=true
mvn deploy -Dsonar.skip=true
```



Dependencies 依赖项项目

- 工具类依赖项 parent-util-dependencies
- 中间件 parent-middleware-dependencies