<groupId>pers.vic</groupId>
	<artifactId>vic-boot-archtype</artifactId>
	<version>1.0.0-SNAPSHOT</version>
	<name>一个简单的Archetype</name>
	<description>一个简单的springboot Archetype</description>[toc]
## 自定义maven archtype

#### 一 新建maven项目 vic-boot-archtype

```xml
<project xmlns="http://maven.apache.org/POM/4.0.0"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 https://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>
	
	<groupId>pers.vic</groupId>
	<artifactId>vic-boot-archtype</artifactId>
	<version>1.0.0-SNAPSHOT</version>
	<name>一个简单的Archetype</name>
	<description>一个简单的springboot Archetype</description>

	<build>
		<plugins>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-archetype-plugin</artifactId>
				<version>3.1.2</version>
			</plugin>
		</plugins>
	</build>
</project>
```



#### 二 在 src/main/resources下新建文件夹:

- src/main/resources
  - archetype-resources: 在此文件下就是最终生成项目时候的所有原始文件
    - pom.xmp
  - META-INF
    - maven
      - `archetype-metadata.xml`:   Archetype的描述文件

##### 2.1 archetype-resources: 在此文件下就是最终生成项目时候的所有原始文件

* 如pom.xml
* src/main/java
* src/test/java
* src/mian/resources
* 等等

##### 2.2META-INF/maven下`archetype-metadata.xml`文件: Archetype的描述文件

***archetype-metadata.xml***:

```
<?xml version="1.0" encoding="UTF-8"?>
<archetype-descriptor name="vic-springboot">
    <fileSets>
        <!--fileSet对应一个目录以及该目录相关的包含或排除规则-->
        <!--filtered表示是否对该文件及合应用属性替换，像${x}这样的内容是否替换成命令行输入的x参数的值-->
        <!--packaged表示是否将该目录下的内容放到生成项目的包路径下-->
        <fileSet filtered="true" packaged="true">
            <!--src/main/java对应archetype-resources/src/main/java-->
            <directory>src/main/java</directory>
            <!--只包含archetype-resources/src/main/java下的.java文件-->
            <include>**/*.java</include>
        </fileSet>
        
        <!--resources一般没有包路径，所以packaged设置为false-->
        <fileSet filtered="true" packaged="false">
            <directory>src/main/resources</directory>
           <includes>
                <include>**/*.*</include>
            </includes>
        </fileSet>

        <fileSet filtered="true" packaged="true">
            <directory>src/test/java</directory>
            <include>**/*.java</include>
        </fileSet>
    </fileSets>

    <requiredProperties>
        <!--使用archetype时候必须要求输入的参数，archetype中可以使用${port}获取-->
        <requiredProperty key="port">
        	<defaultValue>10081</defaultValue>
        </requiredProperty>
        <requiredProperty key="groupId">
            <!--可以设置默认值，使用archetype会使用默认值-->
            <defaultValue>pers.vic</defaultValue>
        </requiredProperty>
    </requiredProperties>
</archetype-descriptor>


```



#### 三 打包项目到本地仓库 clean install

#### 四 通过`maven-archetype-plugin`插件更新模版

> [archetype:crawl](http://maven.apache.org/archetype/maven-archetype-plugin/crawl-mojo.html)    在一个指定的Maven库中查找可以的模板，并更新模板目录

运行此命令后在本地仓库生成一个文件`archetype-catalog.xml`

**archetype-catalog.xml**

``` xml
<?xml version="1.0" encoding="UTF-8"?>
<archetype-catalog xsi:schemaLocation="http://maven.apache.org/plugins/maven-archetype-plugin/archetype-catalog/1.0.0 http://maven.apache.org/xsd/archetype-catalog-1.0.0.xsd"
    xmlns="http://maven.apache.org/plugins/maven-archetype-plugin/archetype-catalog/1.0.0"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
  <archetypes>
    <archetype>
      <groupId>pers.vic</groupId>
      <artifactId>vic-boot-archtype</artifactId>
      <version>1.0.0</version>
      <description>vic-springboot</description>
    </archetype>
  </archetypes>
</archetype-catalog>

```

#### 五  在eclipse配置模版

> Preferences >> Maven >> archetypes >>  add local catalog..... 选在步骤四中的文件

在新建maven project的时候就可以直接使用此模版了;

##### 六 上传到本地私服

> 由于本人本地未搭建私服仓库, 故未验证此步骤

1. setting.xml文件中配置servers节点
2. 在pom.xml中加入repository节点
3. mvn deploy

