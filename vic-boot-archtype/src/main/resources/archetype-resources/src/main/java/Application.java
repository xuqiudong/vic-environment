package pers.vic.${artifactId};

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 *  @description: 启动类
 *  @author Vic.xu
 *  @date: 2020年5月6日下午3:12:43
 */
@SpringBootApplication
@MapperScan(basePackages = "pers.vic.${artifactId}.**.mapper")
public class Application {

	 public static void main(String[] args) {
			SpringApplication.run(Application.class, args);
	}
}
